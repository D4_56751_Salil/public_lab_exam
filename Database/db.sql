/* Containerize MySQL and create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) table inside that. Movie table will be cretaed through db.sql file. */


CREATE database mydb;

CREATE TABLE movie 
(
    movie_id integer primary key auto_increment,
    movie_release_date varchar(30),
    movie_time varchar(20),
    director_name varchar(30)

);


/* Dummy insert for testing:  */

INSERT into movie values( default , '17 nov' , '2:00 pm' , 'qwerty' );

INSERT into movie values( default , '21 dec' , '6:00 pm' , 'abcdef' );


update movie SET director_name = 'ttttt'  movie_time ="10: pm" where  movie_id = 1;