const express = require("express")
const utils = require("../utils")
const db = require("../db");
const { route } = require("express/lib/application");
const { response } = require("express");

const router = express.Router();



// Method for getting all the movie:
router.get("/" , (request , response)=> 
{
    const connection = db.openConnection();

    const statement =
   `SELECT * FROM movie`

    connection.query(statement , (error , result)=>
    {
        connection.end();
        if(result.length >0)
        {
            console.log(result)
            response.send(utils.createResult(error,result))
        }
        else
        {
            response.send("not found!!!")
        }
    })

})


// Method to ADD Movie data into Containerized MySQL table:

router.post('/add', (request , response)=>
{
 const {movie_release_date , movie_time , director_name} = request.body

 const connection = db.openConnection();

 const statement =
 `
 INSERT INTO movie ( movie_release_date , movie_time,director_name) 
 values 
 ('${movie_release_date}' ,'${movie_time}' , '${director_name}')
 `;
 
 connection.query(statement , (error , result)=>
 {
     connection.end();
     response.send(utils.createResult(error,result));
 })
 

})


// UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table

router.put("/update/:id" , (request , response)=>
{
    const connection = db.openConnection();
    const {id} = request.params
    const {releaseDate}  = request.body
    const {movieTimme} = request.body

    const statement =
    `
    UPDATE  movie set  movie_release_date ='${releaseDate}' , movie_time = '${movieTimme}' where movie_id = ${id}
    
    `;

    connection.query(statement , (error , result)=>
    {
        connection.end();
        response.send(utils.createResult(error,result));
    })

    


})


// DELETE --> Delete Movie from Containerized MySQL

router.delete("/remove/:id" , (request , response)=>
{


    const {id} = request.params;
    const connection = db.openConnection();

    const statement = 
    `
    DELETE FROM movie where  movie_id = ${id};
    
    `

    connection.query(statement , (error , result)=>
    {
        connection.end();
        response.send(utils.createResult(error,result));
    })


})




module.exports = router;