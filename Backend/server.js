const express = require('express')

const app = express()

const movieRoute = require("./routes/movieRoute")
const cors = require("cors")

app.use(express.json())


app.use("/movie" , movieRoute)


app.listen(4000,(request, response)=>
{
    console.log("server started on port number 4000")
})